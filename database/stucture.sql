-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema user
-- -----------------------------------------------------

CREATE SCHEMA IF NOT EXISTS `user` DEFAULT CHARACTER SET utf8 ;
USE `user` ;

-- -----------------------------------------------------
-- Table `user`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user`.`user` (
  `idUser` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `userRole` VARCHAR(45) NOT NULL COMMENT '',
  `userLogin` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `userPassword` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `userName` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `userSurname` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `userPatronymic` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `Usercol` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idUser`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `user`.`subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user`.`subject` (
  `idsubject` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `subjectName` VARCHAR(45) NOT NULL COMMENT '',
  `User_idProfessor` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`idsubject`)  COMMENT '',
  INDEX `fk_Subject_User1_idx` (`User_idProfessor` ASC)  COMMENT '',
  CONSTRAINT `fk_Subject_User1`
    FOREIGN KEY (`User_idProfessor`)
    REFERENCES `user`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `user`.`mark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user`.`mark` (
  `idMark` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `Mark` VARCHAR(45) NULL DEFAULT NULL COMMENT '',
  `Characterisctic` TEXT NULL DEFAULT NULL COMMENT '',
  `User_idStudent` INT(11) NOT NULL COMMENT '',
  `Subject_idsubject` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`idMark`)  COMMENT '',
  INDEX `fk_Mark_User_idx` (`User_idStudent` ASC)  COMMENT '',
  INDEX `fk_Mark_Subject1_idx` (`Subject_idsubject` ASC)  COMMENT '',
  CONSTRAINT `fk_Mark_Subject1`
    FOREIGN KEY (`Subject_idsubject`)
    REFERENCES `user`.`subject` (`idsubject`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Mark_User`
    FOREIGN KEY (`User_idStudent`)
    REFERENCES `user`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `user`.`subjectstudent`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user`.`subjectstudent` (
  `idSubjectStudent` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `Subject_idsubject` INT(11) NOT NULL COMMENT '',
  `User_idStudent` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`idSubjectStudent`)  COMMENT '',
  INDEX `fk_SubjectStudent_Subject1_idx` (`Subject_idsubject` ASC)  COMMENT '',
  INDEX `fk_SubjectStudent_User1_idx` (`User_idStudent` ASC)  COMMENT '',
  CONSTRAINT `fk_SubjectStudent_Subject1`
    FOREIGN KEY (`Subject_idsubject`)
    REFERENCES `user`.`subject` (`idsubject`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SubjectStudent_User1`
    FOREIGN KEY (`User_idStudent`)
    REFERENCES `user`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
