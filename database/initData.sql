USE user;

-- -----------------------------------------------------
-- Удаление данных из БД
-- -----------------------------------------------------
SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE TABLE user;
TRUNCATE TABLE mark;
TRUNCATE TABLE subject;
TRUNCATE TABLE subjectstudent;
SET FOREIGN_KEY_CHECKS = 1;

-- -----------------------------------------------------
-- Добавление данных в БД
-- -----------------------------------------------------

INSERT INTO User (`idUser`, `userRole`, `userLogin`, `userPassword`, `userName`, `userSurname`, `userPatronymic`) VALUES (1, 'Student', '1', '1', 'Vasya', 'Vasyliev', 'Vyacheslavovich');
INSERT INTO User (`idUser`, `userRole`, `userLogin`, `userPassword`, `userName`, `userSurname`, `userPatronymic`) VALUES (2, 'Professor', '2', '2', 'Igor', 'Klimov', 'Zinonovich');
INSERT INTO User (`idUser`, `userRole`, `userLogin`, `userPassword`, `userName`, `userSurname`, `userPatronymic`) VALUES (3, 'Professor', '3', '3', 'Svetlana', 'Bondareva', 'Vladimirovna');
INSERT INTO User (`idUser`, `userRole`, `userLogin`, `userPassword`, `userName`, `userSurname`, `userPatronymic`) VALUES (4, 'Student', '4', '4', 'Petya', 'Petrov', 'Petrovich');

INSERT INTO subject (`idsubject`, `subjectName`, `User_idProfessor`) VALUES (1, 'RLS', '2');
INSERT INTO subject (`idsubject`, `subjectName`, `User_idProfessor`) VALUES (2, 'Fizika', '3');
INSERT INTO subject (`idsubject`, `subjectName`, `User_idProfessor`) VALUES (3, 'Radiotehnika', '2');

INSERT INTO subjectstudent (`idSubjectStudent`, `Subject_idsubject`, `User_idStudent`) VALUES (1, 1, 1);
INSERT INTO subjectstudent (`idSubjectStudent`, `Subject_idsubject`, `User_idStudent`) VALUES (2, 2, 1);
INSERT INTO subjectstudent (`idSubjectStudent`, `Subject_idsubject`, `User_idStudent`) VALUES (3, 1, 4);
INSERT INTO subjectstudent (`idSubjectStudent`, `Subject_idsubject`, `User_idStudent`) VALUES (4, 3, 4);

INSERT INTO mark (`idMark`, `Mark`, `Characterisctic`, `User_idStudent`, `Subject_idsubject`) VALUES (1, 4, 'Ok1', 1, 1);
INSERT INTO mark (`idMark`, `Mark`, `Characterisctic`, `User_idStudent`, `Subject_idsubject`) VALUES (2, 5, 'Ok2', 4, 3);