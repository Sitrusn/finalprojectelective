<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>StudentPage</title>
	<link rel="stylesheet" href="css/studentPage.css">
</head>

<body>
<header>

	<div class="title">
		ФАКУЛЬТАТИВ
	</div>

	<div class="greeting">
		<p>
				Добро пожаловать в систему "Факультатив".
			Выберите, пожалуйста, предметы, которые хотите изучить.
		</p>
	</div>

	<table class="tableTitle">
		<tr>
			<td>ПРЕДМЕТЫ</td>
			<td width="160px">ПРЕПОДАВАТЕЛИ</td>
		</tr>
	</table>

    <form action="/student" method="post">
        <fieldset class="fieldset">
            <table class="subjectsTable">
                <c:forEach items="${studentStringContainer.studentStrings}" var="studentString" varStatus="status">
                    <tr>
                        <td><input name="selectedSubjects"    type="checkbox"              value=${studentString.subjectName}                       </td>
                        <td><input name="studentStrings[${status.index}].subjectName"      value=${studentString.subjectName}           readonly    </td>
                        <td><input name="studentStrings[${status.index}].userSurname"      value=${studentString.userSurname}           readonly    </td>
                        <td><input name="studentStrings[${status.index}].userName"         value=${studentString.userName}              readonly    </td>
                        <td><input name="studentStrings[${status.index}].userPatronymic"   value=${studentString.userPatronymic}        readonly    </td>
                    </tr>
                </c:forEach>
            </table>
            <input class="sendButton" type="submit" value="Отправить">
        </fieldset>
    </form>

</header>
</body>
</html>