<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>registration</title>
	<link rel="stylesheet" href="css/registration.css">
</head>

<body>
<header>

	<div class="title">
		РЕГИСТРАЦИЯ
	</div>

	<form action="/registration" class="form" method="post">
		<fieldset class="fieldset">
			<legend>Ввод данных:</legend>
			<table class="table">
		    	<tr>
					<td>Фамилия:</td>
					<td>Роль:</td>
				<tr>
					<td><input type="text" name="userSurname"></td>
					<td>
						<select class ="select" name = "userRole">
							<option value = "Professor">Преподаватель</option>
							<option value = "Student">Студент</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Имя:</td>
					<td>Логин:</td>
				</tr>
				<tr>
					<td><input type="text" name="userName"></td>
					<td><input type="text" name="userLogin"></td>
				</tr>
				<tr>
					<td>Отчество:</td>
					<td>Пароль:</td>
				</tr>
				<tr>
					<td><input type="text" name="userPatronymic"></td>
					<td><input type="text" name="userPassword"></td>
				</tr>
			</table>
			<p><input type="submit" value="Отправить" align="right" class="sendButton"></p>
		</fieldset>
	</form>


</header>

</body>
</html>