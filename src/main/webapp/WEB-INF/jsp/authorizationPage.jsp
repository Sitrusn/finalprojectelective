<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>authorization</title>
    <link rel="stylesheet" href="/css/index.css">
</head>

<body>
<header>
    <form action="/authorization" method="post">

	    <div class="title">
		    АВТОРИЗАЦИЯ
	    </div>
	    <div class="authorizationForm">
		    Логин:<br>
		    <input type="text" name="userLogin"><br>
		    Пароль:<br>
		    <input type="text" name="userPassword">
	    </div>

	    <div class="button">
			<input type="submit" value="Вход">
		</div>
	</form>

</header>
</body>
</html>