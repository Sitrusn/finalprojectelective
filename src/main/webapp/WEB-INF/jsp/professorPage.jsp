<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>LecturerPage</title>
	<link rel="stylesheet" href="css/lecturerPage.css">
</head>

<body>
	<header>
		<div class="title">
			ФАКУЛЬТАТИВ
		</div>

		<div class="greeting">
			Наш университет славится честными преподавателями
		</div>
	</header>

	<ul class="menu">
	    <li>Предмет    &emsp;&emsp;&emsp;&emsp;&emsp;              </li>
		<li>Фамилия    &emsp;&emsp;&emsp;&emsp;&emsp;              </li>
		<li>Имя        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;  </li>
		<li>Отчество   &emsp;&emsp;&emsp;&emsp;&emsp;              </li>
		<li>Оценка     &emsp;&emsp;&emsp;&emsp;&emsp;&ensp;        </li>
		<li>Характеристика студента                                </li>
	</ul>

    <form action="/professor" method="post">
        <table>
            <c:forEach items="${professorStringContainer.professorStrings}" var="professorString" varStatus="status">
                <tr>
                    <td><input name="professorStrings[${status.index}].subjectName"       value=${professorString.subjectName}           readonly    </td>
                    <td><input name="professorStrings[${status.index}].userName"          value=${professorString.userName}              readonly    </td>
                    <td><input name="professorStrings[${status.index}].userSurname"       value=${professorString.userSurname}           readonly    </td>
                    <td><input name="professorStrings[${status.index}].userPatronymic"    value=${professorString.userPatronymic}        readonly    </td>
                    <td><input name="professorStrings[${status.index}].mark"              value=${professorString.mark}                              </td>
                    <td><input name="professorStrings[${status.index}].characterisctic"   value=${professorString.characterisctic}       size="70"   </td>
                </tr>
            </c:forEach>
        </table>
        <p><input type="submit" value="Подтвердить" class="sendButton"></p>
    </form>

</body>
</html>