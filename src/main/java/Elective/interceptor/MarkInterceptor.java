package Elective.interceptor;

import Elective.dto.ProfessorString;
import Elective.dto.ProfessorStringContainer;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

public class MarkInterceptor implements HandlerInterceptor {

    public void postHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

        Object model =  modelAndView.getModel().get("professorStringContainer");
        List<ProfessorString> professorStrings = ((ProfessorStringContainer) model).getProfessorStrings();

        List<String> possibleMarks = Arrays.asList("2", "3", "4", "5");
        String status = "good";

        Integer strNum = -1;
        for (ProfessorString k: professorStrings) {
            strNum++;
            if( !possibleMarks.contains(k.getMark()) ) {
                status = "bad";
                break;
            }
        }

        switch (status) {
            case "good":
                modelAndView.setViewName("professorPage");
                break;
            case "bad":
                professorStrings.get(strNum).setMark("XXX");
                modelAndView.setViewName("professorPage");
                break;
        }
    }

}
