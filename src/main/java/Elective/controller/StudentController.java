package Elective.controller;

import Elective.dao.StudentDAO;
import Elective.dto.StudentStringContainer;
import Elective.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;

@Controller
@SessionAttributes("user")
public class StudentController {

    @Autowired
    StudentDAO studentDAO;

    @GetMapping("/student")
    public ModelAndView getStudentPage(@ModelAttribute User user) {
        ModelAndView modelAndView = new ModelAndView();
        StudentStringContainer studentStrings = new StudentStringContainer();
        studentStrings.setStudentStrings(studentDAO.getStudentStrings(user));

        modelAndView.addObject(user);
        modelAndView.addObject("studentStringContainer", studentStrings);
        modelAndView.setViewName("studentPage");

        return modelAndView;
    }

    @PostMapping("/student")
    public ModelAndView setStudentPage(HttpServletRequest request, @ModelAttribute("user") User user) {
        ModelAndView modelAndView = new ModelAndView();
        String[] selectedSubjects = request.getParameterValues("selectedSubjects");

        for (String str: selectedSubjects){
            studentDAO.setSelectedSubjectsToDB(user, str);
        }

        modelAndView.setViewName("goodWorkPage");
        return modelAndView;
    }
}