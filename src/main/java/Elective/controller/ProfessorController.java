package Elective.controller;

import Elective.dao.ProfessorDAO;
import Elective.dto.ProfessorStringContainer;
import Elective.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("user")
public class ProfessorController {

    @Autowired
    ProfessorDAO professorDAO;

    @GetMapping("/professor")
    public ModelAndView setProfessorPage(@ModelAttribute User user) {
        ModelAndView modelAndView = new ModelAndView();
        ProfessorStringContainer professorStrings = new ProfessorStringContainer();
        professorStrings.setProfessorStrings(professorDAO.getProfessorStrings(user));
        System.out.println("Контроллер GetMapping");
        return new ModelAndView("professorPage", "professorStringContainer", professorStrings);
    }

    @PostMapping("/professor")
    public ModelAndView getProfessorPage(ProfessorStringContainer professorStrings) {
        professorDAO.setProfessorStrings(professorStrings);
        return new ModelAndView("professorPage", "professorStringContainer", professorStrings);

    }
}
