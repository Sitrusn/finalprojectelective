package Elective.controller;

import Elective.dao.ProfessorDAO;
import Elective.dao.StudentDAO;
//import Elective.dao.UserDAO;
import Elective.dto.User;
import Elective.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegistrationController {

    @Autowired
    UserService userService;

    @Autowired
    StudentDAO studentDAO;

    @Autowired
    ProfessorDAO professorDAO;

    @GetMapping("/registration")
    public ModelAndView registrationPage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("registrationPage");
        return modelAndView;
    }

    @PostMapping("/registration")
    public ModelAndView setUserToDB(User user) {
        ModelAndView modelAndView = new ModelAndView();

        switch (user.getUserRole()) {
            case "Student":
                studentDAO.setUserToDB(user);
                break;
            case "Professor":
                professorDAO.setUserToDB(user);
                break;
        }

    return modelAndView;
    }
}