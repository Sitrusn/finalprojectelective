package Elective.controller;

import Elective.dto.User;
import Elective.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class AuthorizationController {

    @Autowired
    UserService userService;

    @GetMapping("/authorization")
    public ModelAndView authorizationPage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("authorizationPage");
        return modelAndView;
    }

    @PostMapping("/authorization")
    public RedirectView getUserByLoginAndPassword(User user, RedirectAttributes redirectAttribute) {
        ModelAndView modelAndView = new ModelAndView();

        redirectAttribute.addFlashAttribute("user", user);
        RedirectView redirectView = new RedirectView();

        userService.setCurrentUser(user);

        if (userService.authorization(user)) {
            switch (userService.getUserRole()) {
                case "Student":
                    redirectView.setUrl("/student");
                    break;
                case "Professor":
                    redirectView.setUrl("/professor");
                    break;
            }
        } else {
            redirectView.setUrl("/registration");
        }
        return redirectView;
    }

}
