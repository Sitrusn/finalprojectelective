package Elective.interfaces;

import Elective.dto.User;

public interface IUserDAO {

    User getUserByLoginAndPassword(User user);

}
