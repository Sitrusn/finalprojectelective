package Elective.interfaces;

import Elective.dto.StudentString;
import Elective.dto.User;

import java.util.List;

public interface IStudentDAO {

    void setUserToDB(User user);

    List<StudentString> getStudentStrings(User user);

    void setSelectedSubjectsToDB(User user, String str);
}
