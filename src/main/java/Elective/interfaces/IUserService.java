package Elective.interfaces;

import Elective.dto.User;

public interface IUserService {

    User setCurrentUser(User user);

    boolean authorization(User user);

    String getUserRole();
}
