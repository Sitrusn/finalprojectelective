package Elective.interfaces;

import Elective.dto.ProfessorString;
import Elective.dto.ProfessorStringContainer;
import Elective.dto.User;

import java.util.List;

public interface IProfessorDAO {
    void setUserToDB(User user);

    List<ProfessorString> getProfessorStrings(User user);

    void setProfessorStrings(ProfessorStringContainer professorStrings);
}
