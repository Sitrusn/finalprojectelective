package Elective.interceptorConfigurator;

import Elective.interceptor.MarkInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

@Configuration
public class InterceptorConfigurator implements WebMvcConfigurer {

    @Bean
    MarkInterceptor markInterceptor() {
        return new MarkInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(markInterceptor()).addPathPatterns("/professor");
    }
}