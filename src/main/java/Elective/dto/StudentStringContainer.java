package Elective.dto;

import java.util.List;

public class StudentStringContainer {

    List<StudentString> studentStrings;

    public List<StudentString> getStudentStrings() {
        return studentStrings;
    }

    public void setStudentStrings(List<StudentString> studentStrings) {
        this.studentStrings = studentStrings;
    }
}
