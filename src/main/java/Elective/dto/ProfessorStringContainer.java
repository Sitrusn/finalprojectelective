package Elective.dto;

import java.util.List;

public class ProfessorStringContainer {

    List<ProfessorString> professorStrings;

    public List<ProfessorString> getProfessorStrings() {
        return professorStrings;
    }

    public void setProfessorStrings(List<ProfessorString> professorStrings) {
        this.professorStrings = professorStrings;
    }
}
