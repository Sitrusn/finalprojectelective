package Elective.dto;

import org.springframework.stereotype.Component;

@Component
public class User {

    private Integer idUser;
    private String userRole;
    private String userName;
    private String userSurname;
    private String userPatronymic;
    private String userLogin;
    private String userPassword;

    public User() {
    }

    public User(Integer idUser, String userName, String userSurname, String userPatronymic, String userRole, String userLogin, String userPassword) {
        this.userName = userName;
        this.userSurname = userSurname;
        this.userPatronymic = userPatronymic;
        this.userRole = userRole;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
    }

    public Integer getIdUser(){
        return idUser;
    }
    public void setIdUser(Integer idUser){
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }
    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public String getUserPatronymic() {
        return userPatronymic;
    }
    public void setUserPatronymic(String userPatronymic) {
        this.userPatronymic = userPatronymic;
    }

    public String getUserRole() {
        return userRole;
    }
    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserLogin() {
        return userLogin;
    }
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

}