package Elective.dto;

public class ProfessorString {//Строка, выводимая на экране профессора. Всё.

    String subjectName;
    String userName;
    String userSurname;
    String userPatronymic;
    String mark;
    String characterisctic;

    public ProfessorString() {
    }

    public ProfessorString(String subjectName, String userName, String userSurname, String userPatronymic, String mark, String characterisctic) {
        this.subjectName = subjectName;
        this.userName = userName;
        this.userSurname = userSurname;
        this.userPatronymic = userPatronymic;
        this.mark = mark;
        this.characterisctic = characterisctic;
    }

    public String getSubjectName(){
        return subjectName;
    }
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getUserName (){
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname (){
        return userSurname;
    }
    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public String getUserPatronymic (){
        return userPatronymic;
    }
    public void setUserPatronymic(String userPatronymic) {
        this.userPatronymic = userPatronymic;
    }

    public String getMark (){
        return mark;
    }
    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getCharacterisctic (){
        return characterisctic;
    }
    public void setCharacterisctic(String characterisctic) {
        this.characterisctic = characterisctic;
    }

}

