package Elective.dto;

public class StudentString {//Строка, выводимая на экране студента. Всё.

    String subjectName;
    String userName;
    String userSurname;
    String userPatronymic;

    public StudentString(){
    }

    public StudentString(String subjectName, String userName, String userSurname, String userPatronymic) {
        this.subjectName = subjectName;
        this.userName = userName;
        this.userSurname = userSurname;
        this.userPatronymic = userPatronymic;
    }

    public String getSubjectName(){
        return subjectName;
    }
    public void setSubjectName(String subjectName){
        this.subjectName = subjectName;
    }

    public String getUserName(){
        return userName;
    }
    public void setUserName(String userName){
        this.userName = userName;
    }

    public String getUserSurname(){
        return userSurname;
    }
    public void setUserSurname(String userSurname){
        this.userSurname = userSurname;
    }

    public String getUserPatronymic(){
        return userPatronymic;
    }
    public void setUserPatronymic(String userPatronymic){
        this.userPatronymic = userPatronymic;
    }

}
