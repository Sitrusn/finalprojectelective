package Elective.service;

import Elective.interfaces.IUserService;
import Elective.dao.UserDAO;
import Elective.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

@Service
@SessionAttributes("user")
public class UserService implements IUserService {

    @Autowired
    UserDAO userDAO;

    private User currentUser;

    @ModelAttribute("user")
    public User setCurrentUser(User user){
        currentUser = userDAO.getUserByLoginAndPassword(user);
        if(currentUser != null) {
            user.setIdUser(currentUser.getIdUser());
            user.setUserRole(currentUser.getUserRole());
            user.setUserName(currentUser.getUserName());
            user.setUserLogin(currentUser.getUserLogin());
            user.setUserSurname(currentUser.getUserSurname());
            user.setUserPassword(currentUser.getUserPassword());
            user.setUserPatronymic(currentUser.getUserPatronymic());
        }
        return currentUser;
    }

    public boolean authorization(User user) {
        currentUser = userDAO.getUserByLoginAndPassword(user);
        if (currentUser != null) {
            return true;
        } else {
            return false;
        }
    }

    public String getUserRole() {
        return currentUser.getUserRole();
    }

}