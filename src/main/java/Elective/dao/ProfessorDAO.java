package Elective.dao;

import Elective.interfaces.IProfessorDAO;
import Elective.dto.ProfessorString;
import Elective.dto.ProfessorStringContainer;
import Elective.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class ProfessorDAO implements IProfessorDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<ProfessorString> rowMapper = new BeanPropertyRowMapper<>(ProfessorString.class);

    public void setUserToDB(User user){
        String sql = "INSERT INTO `user` (`userRole`, `userLogin`, `userPassword`, `userName`, `userSurname`, `userPatronymic`) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, "Professor", user.getUserLogin(), user.getUserPassword(), user.getUserName(), user.getUserSurname(),
                user.getUserPatronymic());
    }

    public List<ProfessorString> getProfessorStrings(User user){
        String sql = "SELECT subjectName, userName, userSurname, userPatronymic, Mark, Characterisctic \n" +
                     "FROM subject JOIN user JOIN mark \n" +
                     "WHERE idsubject = Subject_idsubject AND idUser = User_idStudent AND User_idProfessor = ?;";
        return jdbcTemplate.query(sql, rowMapper, user.getIdUser());
    }

    public void setProfessorStrings(ProfessorStringContainer professorStrings){
        for (ProfessorString professorString :  professorStrings.getProfessorStrings()) {
        String sql = "UPDATE Mark mark\n" +
                "JOIN Subject ON Subject_idsubject = idsubject \n" +
                "JOIN User ON User_idStudent = idUser\n" +
                "SET mark.Mark = ?, mark.Characterisctic = ?\n" +
                "WHERE subjectName = ? AND userName = ? AND userSurname = ? AND userPatronymic = ?";
        jdbcTemplate.update(sql,  professorString.getMark(), professorString.getCharacterisctic(),
                professorString.getSubjectName(), professorString.getUserName(), professorString.getUserSurname(), professorString.getUserPatronymic());
        }
    }
}
