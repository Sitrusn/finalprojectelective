package Elective.dao;

import Elective.interfaces.IStudentDAO;
import Elective.dto.StudentString;
import Elective.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class StudentDAO implements IStudentDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<StudentString> rowMapper = new BeanPropertyRowMapper<>(StudentString.class);

    public void setUserToDB(User user){
        String sql = "INSERT INTO `user` (`userRole`, `userLogin`, `userPassword`, `userName`, `userSurname`, `userPatronymic`) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, "Student", user.getUserLogin(), user.getUserPassword(), user.getUserName(), user.getUserSurname(),
                user.getUserPatronymic());
    }

//    public List<StudentString> getStudentStrings(User user){  //вывод предметов конкретного студента
//        String sql = "SELECT subjectName, userName, userSurname, userPatronymic " +
//                "FROM subjectstudent JOIN subject JOIN user " +
//                "WHERE Subject_idsubject = idsubject AND User_idProfessor = idUser AND User_idStudent = ?;";
//        return jdbcTemplate.query(sql, rowMapper,user.getIdUser());
//    }

    public List<StudentString> getStudentStrings(User user){
        String sql = "SELECT subjectName, userName, userSurname, userPatronymic " +
                "FROM subject JOIN user " +
                "WHERE User_idProfessor = idUser;";
        return jdbcTemplate.query(sql, rowMapper);
    }

    public void setSelectedSubjectsToDB(User user, String str){
        String sql1 = "DELETE FROM Mark WHERE User_idStudent = ?;";
        jdbcTemplate.update(sql1, user.getIdUser());
        String sql2 = "INSERT INTO `mark` (`User_idStudent`, `Subject_idSubject`) " +
                "VALUES (?, (SELECT idsubject FROM subject WHERE subjectName = ?));";
        jdbcTemplate.update(sql2, user.getIdUser(), str);
    }
}