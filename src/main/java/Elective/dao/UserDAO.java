package Elective.dao;

import Elective.interfaces.IUserDAO;
import Elective.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO implements IUserDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    User currentUser;

    private RowMapper<User> rowMapper = new BeanPropertyRowMapper<>(User.class);

    public User getUserByLoginAndPassword(User user) {
        String sql = "SELECT * FROM user WHERE userLogin = ? AND userPassword = ? ";
        try {
            currentUser = jdbcTemplate.queryForObject(sql, rowMapper, user.getUserLogin(), user.getUserPassword());
            return currentUser;
        } catch(EmptyResultDataAccessException e) {
            currentUser = null;
            return currentUser;
        }
    }

}
